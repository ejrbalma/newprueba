package applicationgit;

public class CalculateHistogram <T> {

    public static <T> Histogram <T> computeHistogram (T [] vector){
        Histogram<T> histo = new Histogram<>();
        for (T key : vector) {
            histo.increment(key);
        }
        return histo;
    }
    
}

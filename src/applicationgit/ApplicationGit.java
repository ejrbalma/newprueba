package applicationgit;

import java.util.HashMap;
import java.util.Map;

public class ApplicationGit {

    public static void main(String[] args) {

        //Integer [] vector = {1, 4, 5, 4, 2, 1, 2, 3, 5, 6, 1, 7};
        String [] vector = {"Ana", "Elena", "Juan", "Enrique","Elena", "Juan","Elena", "Juan","Elena"};

        Histogram<Object> histogram = CalculateHistogram.computeHistogram(vector);
        

        for (Object key : histogram.keySet()) {
            System.out.println(key + " ---> " + histogram.get(key));
        }

   
    }

}

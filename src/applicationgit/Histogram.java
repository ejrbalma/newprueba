package applicationgit;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class Histogram <E>{

private final Map <E, Integer> map = new HashMap<>(); 

    public Integer get(Object key) {
        return map.get(key);
    }

    public Set<E> keySet() {
        return map.keySet();
    }
    
public void increment (E key){
    map.put(key,map.containsKey(key)? map.get(key)+1: 1);
}
    
}
